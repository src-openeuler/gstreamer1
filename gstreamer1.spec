Name:           gstreamer1
Version:        1.24.11
Release:        1
Summary:        GStreamer streaming media framework runtime
License:        LGPL-2.1-or-later
URL:            https://gstreamer.freedesktop.org/
Source0:        https://gstreamer.freedesktop.org/src/gstreamer/gstreamer-%{version}.tar.xz
Source1:        gstreamer1.attr
Source2:        gstreamer1.prov


Patch0001:      0001-gst-inspect-add-mode-to-output-RPM-requires-format.patch

BuildRequires: meson >= 1.1
BuildRequires: pkgconfig(bash-completion) >= 2.0
BuildRequires: pkgconfig(gio-2.0)
BuildRequires: pkgconfig(gio-unix-2.0)
BuildRequires: pkgconfig(glib-2.0) >= 2.64.0
BuildRequires: pkgconfig(gmodule-no-export-2.0)
BuildRequires: pkgconfig(gmp)
BuildRequires: pkgconfig(gobject-2.0)
BuildRequires: pkgconfig(gsl)
BuildRequires: pkgconfig(libcap)
BuildRequires: pkgconfig(libdw)
BuildRequires: pkgconfig(libunwind)
BuildRequires: /usr/bin/g-ir-scanner
BuildRequires: bison >= 2.4
BuildRequires: flex >= 2.5.31
BuildRequires: gettext
BuildRequires: rustc

%description
GStreamer1 implements a framework that allows for processing and encoding of
multimedia sources in a manner similar to a shell pipeline.

Because it's introspection-based, most of the classes follow directly from the
C API. Therefore, most of the documentation is by example rather than a full
breakdown of the class structure.

%package        devel
Summary:        Development files for GStreamer streaming media framework
Requires:       %{name} = %{version}-%{release}

%description    devel
The %{name}-devel package includes the libraries, header files, etc.,
that you'll need to develop applications that are linked with the
gstreamer1 extensibility library.

%package_help

%prep
%autosetup -n gstreamer-%{version} -p3

%build
%meson  \
  -D tests=disabled -D examples=disabled \
  -D ptp-helper-permissions=capabilities -D dbghelp=disabled \
  -D doc=disabled
%meson_build

%install
%meson_install

%find_lang gstreamer-1.0

install -m0644 -D %{S:1} %{buildroot}%{_rpmconfigdir}/fileattrs/gstreamer1.attr
install -m0755 -D %{S:2} %{buildroot}%{_rpmconfigdir}/gstreamer1.prov

%files -f gstreamer-1.0.lang
%doc AUTHORS NEWS
%license COPYING
%{_bindir}/gst-*-1.0
%{_rpmconfigdir}/gstreamer1.prov
%{_rpmconfigdir}/fileattrs/gstreamer1.attr
%{_libdir}/*.so.*
%{_libdir}/gstreamer-1.0/*.so
%{_libdir}/girepository-1.0/*.typelib
%{_libexecdir}/gstreamer-1.0/*
%{_datadir}/bash-completion/helpers/gst
%{_datadir}/bash-completion/completions/gst-*

%files devel
%{_datadir}/aclocal/gst-element-check-1.0.m4
%{_datadir}/gir-1.0/*.gir
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_includedir}/gstreamer-1.0/gst/*.h
%{_includedir}/gstreamer-1.0/gst/net/*.h
%{_includedir}/gstreamer-1.0/gst/controller/*.h
%{_includedir}/gstreamer-1.0/gst/check/*.h
%{_includedir}/gstreamer-1.0/gst/base/*.h

%{_datadir}/gstreamer-1.0/gdb/*
%{_datadir}/gdb/auto-load/*

%files help
%doc RELEASE
%{_mandir}/man1/*

%changelog
* Tue Jan 07 2025 Funda Wang <fundawang@yeah.net> - 1.24.11-1
- update to 1.24.11

* Wed Dec 04 2024 Funda Wang <fundawang@yeah.net> - 1.24.10-1
- update to 1.24.10

* Thu Oct 31 2024 Funda Wang <fundawang@yeah.net> - 1.24.9-1
- update to 1.24.9

* Mon Mar 11 2024 liweigang <liweiganga@uniontech.com> - 1.24.0-1
- update to version 1.24.0

* Wed Nov 22 2023 lwg <liweiganga@uniontech.com> - 1.22.5-1
- update to version 1.22.5

* Tue Nov 1 2022 huyab<1229981468@qq.com> - 1.20.4-1
- update to 1.20.4

* Sat Jun 25 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 1.20.3-1
- update to 1.20.3

* Mon Jun 20 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 1.19.3-2
- fix build when Meson >= 0.61.5
- remove meson option gtk_doc

* Fri Dec 3 2021 hanhui <hanhui15@huawei.com> - 1.19.3-1
- Upgrade to gstreamer-1.19.3

* Wed Jun 23 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 1.18.4-1
- Upgrade to 1.18.4
- Delete Adapt-to-backwards-incompatible-change-in-GUN.patch whose target
  patch file doesn't exist in this version 1.18.4
- Add gstreamer-inspect-rpm-format.patch
- Use meson rebuild

* Wed Mar 3 2021 yanan <yanan@huawei.com> - 1.16.2-3
- remove buildrequires for gtk-doc

* Tue Aug 4 2020 wangye <wangye70@huawei.com> - 1.16.2-2
- fix 1.16.2 make error

* Sat Jul 25 2020 hanhui <hanhui15@huawei.com> - 1.16.2-1
- update 1.16.2

* Tue Jan 7 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.14.4-4
- update software package

* Sun Dec 29 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.14.4-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:optimization the spec

* Tue Aug 27 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.14.4-2
- Package Init


